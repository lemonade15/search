#include <iostream>
#include <string>
#include <vector>

using std::string;

int search (std::string text, std::string pattern)
{
	int i = 0;
	
	for(i = 0; i < text.length(); i++)
	{
		if (text[i] == pattern)
		{
			return i;
		}
	}
	return -1;
}
